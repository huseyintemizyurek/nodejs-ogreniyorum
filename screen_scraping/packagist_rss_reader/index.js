var request = require('request');
var cheerio = require('cheerio');

var url  = 'https://packagist.org/feeds/packages.rss';
var arr  = Array();
var json = {};

request(url, function (error, response, html) {

  if (!error) {
      $ = cheerio.load(html, {
        normalizeWhitespace : true,
        xmlMode : true
      });

      $('channel item').filter(function (el) {

        arr.push(
          $(this).find('title').text().trim()
        );

      });

      json = arr;
      console.log(JSON.stringify(json,null,4));

  }

});
